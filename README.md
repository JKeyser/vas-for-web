# VAS-for-Web

A freely licensed Visual Analog Scale implemented in HTML, CSS, and JavaScript.

## Collection of ideals
- focus on simplicity and practicality
    - layout as close as possible to most common paper versions, to achieve instant plausible comparability
    - data export as a simple list, or CSV file, maybe "share" button (if run on mobile browser)
    - keep it as simple and expandable as possible (at least Icons/Emojis should be easily swappable for different scales)
- should operate on all relevant mobile devices / browsers
- publish with GitLab Pages for self-use and show case
- core code should be freely licensed: CC0? (-> code made from scratch or from compatible sources)
    - make clear if icons are from different source, and check their licenses

## UI ideas
- Use JS to register tap on the scale and show fading dot where tap is registered.
    - As long as the dot is visible, the subject can still adjust it, just like on paper.
    - As soon as the dot faded away, the response is considered set; this method obviates need for a button "OK that's my response".
- The registered results should show up numerically as they arrive, either on a separate (swipe-able?) page or just by scrolling down (out of view for the subject).

## Resources of interest
- https://www.academia.edu/2660006/VAS_Generator_A_Web-Based_Tool_for_Creating_Visual_Analogue_Scales
- https://openmoji.org/library/#group=smileys-emotion
- http://vasgenerator.net/

## Papers of interest
- https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3454549/
- https://www.ncbi.nlm.nih.gov/pubmed/12237213

